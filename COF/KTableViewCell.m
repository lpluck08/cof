//
//  KTableViewCell.m
//  COF
//
//  Created by corptest on 13-8-28.
//  Copyright (c) 2013年 klaus. All rights reserved.
//

#import "KTableViewCell.h"

@implementation KTableViewCell

- (BOOL) canBecomeFirstResponder
{
    [super canBecomeFirstResponder];
    return YES;
}

- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
