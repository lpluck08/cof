//
//  KImageMessageButton.h
//  COF
//
//  Created by corptest on 13-8-27.
//  Copyright (c) 2013年 klaus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KCOFDefine.h"

@interface KImageMessageButton : UIView

@property (strong, nonatomic) UIImage *image;

@property (strong, nonatomic) id userInfo;

- (void) setOnLongPressBlock:(KOneArgBlock)longPressBlock;

- (void) setOnClickBlock:(KOneArgBlock)clickBlock;

@end
