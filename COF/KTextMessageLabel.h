//
//  KLabel.h
//  COF
//
//  Created by corptest on 13-8-27.
//  Copyright (c) 2013年 klaus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KCOFDefine.h"

@interface KTextMessageLabel : UIView

@property (strong, nonatomic) NSString *text;

@property (strong, nonatomic) UIFont *font;

@property (strong, nonatomic) id userInfo;

- (void) setBackgroundColor:(UIColor *)backgroundColor forState:(UIControlState)state;

- (CGSize) sizeToFit;

- (void) setOnLongPressBlock:(KOneArgBlock)longPressBlock;

@end
