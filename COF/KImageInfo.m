//
//  KImageInfo.m
//  COF
//
//  Created by corptest on 13-8-23.
//  Copyright (c) 2013年 klaus. All rights reserved.
//

#import "KImageInfo.h"

@implementation KImageInfo

+ (id) imageInfoWithSize:(CGSize)size imgUrl:(NSString *)imgUrl
{
    KImageInfo *imgInfo = [[KImageInfo alloc] init];
    imgInfo.size = size;
    imgInfo.imgUrl = imgUrl;
    return imgInfo;
}

@end
