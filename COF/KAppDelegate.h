//
//  KAppDelegate.h
//  COF
//
//  Created by corptest on 13-8-19.
//  Copyright (c) 2013年 klaus. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KMainViewController;

@interface KAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) KMainViewController *viewController;

@end
