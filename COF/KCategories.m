//
//  KCategories.m
//  COF
//
//  Created by corptest on 13-8-22.
//  Copyright (c) 2013年 klaus. All rights reserved.
//

#import "KCategories.h"

@implementation UIImage (ClacSize)

// 根据颜色生成图片
+ (UIImage *) imageWithColor:(UIColor *)color size:(CGSize)size
{
    UIImage *img = [[UIImage alloc] init];
    img = [img maskImage:color size:size];
    return img;
}

- (UIImage *)maskImage:(UIColor *)maskColor size:(CGSize)size
{
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    
    UIGraphicsBeginImageContext(rect.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0, rect.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextClipToMask(context, rect, self.CGImage);
    CGContextSetFillColorWithColor(context, maskColor.CGColor);
    CGContextFillRect(context, rect);
    
    UIImage *smallImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return smallImage;
}

@end
