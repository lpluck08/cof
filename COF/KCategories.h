//
//  KCategories.h
//  COF
//
//  Created by corptest on 13-8-22.
//  Copyright (c) 2013年 klaus. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage (ClacSize)

// 根据颜色生成图片
+ (UIImage *) imageWithColor:(UIColor *)color size:(CGSize)size;

@end
