//
//  KMessage.h
//  COF
//
//  Created by corptest on 13-8-22.
//  Copyright (c) 2013年 klaus. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KMessage : NSObject

@property (strong, nonatomic) NSString *msg;

@property (strong, nonatomic) NSArray *expressions;

@property (strong, nonatomic) NSArray *imgs;

@end
