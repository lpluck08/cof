//
//  KInfo.h
//  COF
//
//  Created by corptest on 13-8-20.
//  Copyright (c) 2013年 klaus. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KMessage;

@interface KInfo : NSObject

@property (nonatomic) NSInteger infoId;

@property (strong, nonatomic) NSString *avatarUrl;

@property (strong, nonatomic) NSString *nickname;

@property (strong, nonatomic) KMessage *msg;

@property (strong, nonatomic) NSString *publishDate;

@end
