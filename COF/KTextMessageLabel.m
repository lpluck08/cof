//
//  KLabel.m
//  COF
//
//  Created by corptest on 13-8-27.
//  Copyright (c) 2013年 klaus. All rights reserved.
//

#import "KTextMessageLabel.h"

@interface KTextMessageLabel ()

@property (strong, nonatomic) NSMutableDictionary *backgroundColorDictionary;

@property (strong, nonatomic) NSTimer *changeBackgroundColorTimer;

@property (nonatomic) CGPoint touchBeginPoint;

@property (strong, nonatomic) KOneArgBlock realLongPressBlock;

@end

#define AllowableMovement   10

@implementation KTextMessageLabel

- (BOOL) canBecomeFirstResponder
{
    [super canBecomeFirstResponder];
    return YES;
}

- (id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.font = [UIFont systemFontOfSize:14.];
        self.backgroundColor = [UIColor whiteColor];
        [self setBackgroundColor:self.backgroundColor forState:UIControlStateNormal];
    }
    return self;
}

- (void) drawRect:(CGRect)rect
{
    if (self.text) {
        CGSize size = [self textSize];
        [self.text drawInRect:(CGRect){0, 0, size.width, size.height} withFont:self.font lineBreakMode:NSLineBreakByCharWrapping];
    }
}

- (void) setFont:(UIFont *)font
{
    _font = font;
    [self setNeedsDisplay];
}

- (void) setText:(NSString *)text
{
    _text = text;
    [self setNeedsDisplay];
}

- (CGSize) sizeToFit
{
    if (self.text) {
        CGRect frame = self.frame;
        frame.size = [self textSize];
        self.frame = frame;
        [self setNeedsDisplay];
        return frame.size;
    }
    return CGSizeZero;
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.changeBackgroundColorTimer == nil) {
        self.changeBackgroundColorTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(changeBackgroundColorToHighlight) userInfo:nil repeats:NO];
        self.touchBeginPoint = [[touches anyObject] locationInView:self];
    }
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!CGRectContainsPoint([self allowableMovementBounds], [[touches anyObject] locationInView:self])) {
        [self changeBackgroundColorToNormal];
    }
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self changeBackgroundColorToNormal];
}

- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self changeBackgroundColorToNormal];
}

- (void) setBackgroundColor:(UIColor *)backgroundColor forState:(UIControlState)state
{
    if (state == UIControlStateNormal) {
        self.backgroundColor = backgroundColor;
    }
    if (self.backgroundColorDictionary == nil) {
        self.backgroundColorDictionary = [NSMutableDictionary dictionary];
    }
    [self.backgroundColorDictionary setObject:backgroundColor forKey:[NSString stringWithFormat:@"%d", state]];
}

- (void) setOnLongPressBlock:(KOneArgBlock)longPressBlock;
{
    self.realLongPressBlock = longPressBlock;
}

#pragma mark - 私有方法

- (void) changeBackgroundColorToNormal
{
    if (self.changeBackgroundColorTimer) {
        [self.changeBackgroundColorTimer invalidate];
        self.changeBackgroundColorTimer = nil;
    }
    self.backgroundColor = [self.backgroundColorDictionary objectForKey:[NSString stringWithFormat:@"%d", UIControlStateNormal]];
}

- (void) changeBackgroundColorToHighlight
{
    self.backgroundColor = [self.backgroundColorDictionary objectForKey:[NSString stringWithFormat:@"%d", UIControlStateHighlighted]];
    if (self.realLongPressBlock) {
        self.realLongPressBlock(self);
    }
    self.changeBackgroundColorTimer = nil;
}

- (CGRect) allowableMovementBounds
{
    if (CGPointEqualToPoint(self.touchBeginPoint, CGPointZero)) {
        return CGRectZero;
    }
    return CGRectMake(self.touchBeginPoint.x - AllowableMovement/2, self.touchBeginPoint.y - AllowableMovement/2, AllowableMovement, AllowableMovement);
}

- (CGSize) textSize
{
    return [self.text sizeWithFont:self.font constrainedToSize:CGSizeMake(self.frame.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByCharWrapping];
}

@end
