//
//  main.m
//  COF
//
//  Created by corptest on 13-8-19.
//  Copyright (c) 2013年 klaus. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KAppDelegate class]));
    }
}
