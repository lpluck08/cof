//
//  KCOFDefine.h
//  COF
//
//  Created by corptest on 13-8-21.
//  Copyright (c) 2013年 klaus. All rights reserved.
//

#ifndef COF_KCOFDefine_h
#define COF_KCOFDefine_h

typedef void(^KBasicBlock)(void);

typedef void(^KOneArgBlock)(id arg);

#define K_BACK(block) dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), block)
#define K_MAIN(block) dispatch_async(dispatch_get_main_queue(), block)

#define K_Msg_Image_Max_WH  160 // 消息中的单图片最大高宽

#define K_Msg_Image_WH      70  // 消息中的多图片高宽

#define K_Msg_Font      [UIFont systemFontOfSize:14]

#pragma mark - Colors

#define RGBCOLOR(r,g,b) \
[UIColor colorWithRed:r/256.f green:g/256.f blue:b/256.f alpha:1.f]

#define RGBACOLOR(r,g,b,a) \
[UIColor colorWithRed:r/256.f green:g/256.f blue:b/256.f alpha:a]

#define UIColorFromRGB(rgbValue) \
[UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0x0000FF))/255.0 \
alpha:1.0]

#define UIColorFromRGBA(rgbValue, alphaValue) \
[UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0x0000FF))/255.0 \
alpha:alphaValue]


#endif
