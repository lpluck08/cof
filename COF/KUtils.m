//
//  KUtils.m
//  COF
//
//  Created by corptest on 13-8-22.
//  Copyright (c) 2013年 klaus. All rights reserved.
//

#import "KUtils.h"
#import "KInfo.h"
#import "KMessage.h"
#import "KImageInfo.h"

@implementation KUtils

+ (NSString *) emojiByString:(NSString *)name
{
    if ([name isEqualToString:@"[喜欢]"]) {
        return @"\uE106";
    }
    if ([name isEqualToString:@"[飞吻]"]) {
        return @"\uE418";
    }
    return nil;
}

+ (NSArray *) getFirstData
{
    NSMutableArray *dataArray = [NSMutableArray array];
    
    for (int i = 14; i >= 8; i --) {
        KInfo *info = [[KInfo alloc] init];
        info.infoId = i;
        info.avatarUrl = [NSString stringWithFormat:@"avatar_%d.jpg", i];
        info.nickname = [NSString stringWithFormat:@"昵称%d", i];
        info.publishDate = [NSString stringWithFormat:@"%d分钟前", i];
        
        KMessage *msg = [[KMessage alloc] init];
        NSMutableString *msgString = [NSMutableString stringWithString:@""];
        for (int j = 2; j < ((arc4random() % 5) + 10); j ++) {
            [msgString appendString:@"你好"];
        }
        msg.msg = msgString;
        if (i == 9) {
            msg.expressions = @[@"[飞吻]", @"[喜欢]"];
            
            msg.imgs = @[[KImageInfo imageInfoWithSize:(CGSize){640, 960} imgUrl:@"img_1.jpg"], [KImageInfo imageInfoWithSize:(CGSize){640, 960} imgUrl:@"img_2.jpg"]];
        }
        
        info.msg = msg;
        [dataArray insertObject:info atIndex:0];
    }
    
    return dataArray;
}

+ (NSArray *) getSecondData
{
    NSMutableArray *dataArray = [NSMutableArray array];
    
    for (int i = 7; i >= 1; i --) {
        KInfo *info = [[KInfo alloc] init];
        info.infoId = i;
        info.avatarUrl = [NSString stringWithFormat:@"avatar_%d.jpg", i];
        info.nickname = [NSString stringWithFormat:@"昵称%d", i];
        info.publishDate = [NSString stringWithFormat:@"%d分钟前", i];
        
        KMessage *msg = [[KMessage alloc] init];
        NSMutableString *msgString = [NSMutableString stringWithString:@""];
        for (int j = 2; j < ((arc4random() % 5) + 10); j ++) {
            [msgString appendString:@"中国"];
        }
        msg.msg = msgString;
        if (i == 3) {
            msg.imgs = @[[KImageInfo imageInfoWithSize:(CGSize){598, 900} imgUrl:@"img_3.jpg"]];
        }
        
        info.msg = msg;
        [dataArray insertObject:info atIndex:0];
    }
    
    return dataArray;
}

@end
