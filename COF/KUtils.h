//
//  KUtils.h
//  COF
//
//  Created by corptest on 13-8-22.
//  Copyright (c) 2013年 klaus. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KUtils : NSObject

+ (NSString *) emojiByString:(NSString *)name;

+ (NSArray *) getFirstData;

+ (NSArray *) getSecondData;

@end
