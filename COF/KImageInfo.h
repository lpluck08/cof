//
//  KImageInfo.h
//  COF
//
//  Created by corptest on 13-8-23.
//  Copyright (c) 2013年 klaus. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KImageInfo : NSObject

+ (id) imageInfoWithSize:(CGSize)size imgUrl:(NSString *)imgUrl;

@property (nonatomic) CGSize size;

@property (strong, nonatomic) NSString *imgUrl;

@end
