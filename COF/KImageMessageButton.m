//
//  KImageMessageButton.m
//  COF
//
//  Created by corptest on 13-8-27.
//  Copyright (c) 2013年 klaus. All rights reserved.
//

#import "KImageMessageButton.h"

@interface KImageMessageButton ()

@property (strong, nonatomic) UIButton *bgButton;

@property (strong, nonatomic) NSTimer *longPressTimer;

@property (strong, nonatomic) NSTimer *highlightTimer;

@property (nonatomic) CGPoint touchBeginPoint;

@property (strong, nonatomic) KOneArgBlock realLongPressBlock;

@property (strong, nonatomic) KOneArgBlock realClickBlock;

@end

#define AllowableMovement   10

@implementation KImageMessageButton

- (BOOL) canBecomeFirstResponder
{
    [super canBecomeFirstResponder];
    return YES;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.bgButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.bgButton.frame = frame;
        self.bgButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void) willMoveToSuperview:(UIView *)newSuperview
{
    [newSuperview addSubview:self.bgButton];
    [super willMoveToSuperview:newSuperview];
}

- (void) removeFromSuperview
{
    [self.bgButton removeFromSuperview];
    [super removeFromSuperview];
}

- (void) setImage:(UIImage *)image
{
    if (_image != image) {
        [self.bgButton setImage:image forState:UIControlStateNormal];
    }
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"touchesBegan");
    if (self.longPressTimer == nil) {
        self.longPressTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(longPress) userInfo:nil repeats:NO];
    }
    if (self.highlightTimer == nil) {
        self.highlightTimer = [NSTimer scheduledTimerWithTimeInterval:.5 target:self selector:@selector(stateToHighlight) userInfo:nil repeats:NO];
    }
    self.touchBeginPoint = [[touches anyObject] locationInView:self];
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"touchesMoved");
    if (!CGRectContainsPoint([self allowableMovementBounds], [[touches anyObject] locationInView:self])) {
        self.bgButton.highlighted = NO;
        [self cancelAllTimer];
    }
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"touchesEnded");
    if (CGRectContainsPoint([self allowableMovementBounds], [[touches anyObject] locationInView:self]) && self.longPressTimer && [self.longPressTimer isValid]) {
        // 触发点击事件
        self.realClickBlock(self);
    }
    self.bgButton.highlighted = NO;
    [self cancelAllTimer];
}

- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"touchesCancelled");
    self.bgButton.highlighted = NO;
    [self cancelAllTimer];
}

- (void) setOnLongPressBlock:(KOneArgBlock)longPressBlock;
{
    self.realLongPressBlock = longPressBlock;
}

- (void) setOnClickBlock:(KOneArgBlock)clickBlock
{
    self.realClickBlock = clickBlock;
}

#pragma mark - 私有方法

- (void) stateToHighlight
{
    self.bgButton.highlighted = YES;
}

- (void) longPress
{
    if (self.realLongPressBlock) {
        self.realLongPressBlock(self);
    }
    if ([self.longPressTimer isValid]) {
        [self.longPressTimer invalidate];
    }
    self.longPressTimer = nil;
}

- (void) cancelAllTimer
{
    if ([self.highlightTimer isValid]) {
        [self.highlightTimer invalidate];
    }
    self.highlightTimer = nil;
    if ([self.longPressTimer isValid]) {
        [self.longPressTimer invalidate];
    }
    self.longPressTimer = nil;
}

- (CGRect) allowableMovementBounds
{
    if (CGPointEqualToPoint(self.touchBeginPoint, CGPointZero)) {
        return CGRectZero;
    }
    return CGRectMake(self.touchBeginPoint.x - AllowableMovement/2, self.touchBeginPoint.y - AllowableMovement/2, AllowableMovement, AllowableMovement);
}

@end
