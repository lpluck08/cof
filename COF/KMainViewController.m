//
//  KMainViewController.m
//  COF
//
//  Created by corptest on 13-8-19.
//  Copyright (c) 2013年 klaus. All rights reserved.
//

#import "KMainViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "KCOFDefine.h"
#import "KCategories.h"
#import "KButton.h"
#import "KUtils.h"
#import "KInfo.h"
#import "KMessage.h"
#import "KImageInfo.h"
#import "KImageMessageButton.h"
#import "KTextMessageLabel.h"
#import "KTableViewCell.h"

@interface KMainViewController () {
    CGFloat     lastY;
    BOOL        loading;
}

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) UIImageView *loadMoreImageView;

@property (strong, nonatomic) NSMutableArray *dataArray;

@property (strong, nonatomic) NSArray *insertDataArray;

// 评论相关view

@property (strong, nonatomic) UIView *commentView;

@property (strong, nonatomic) UIView *buttonBgView;

@property (strong, nonatomic) KButton *commentButton;

@property (strong, nonatomic) KButton *likeButton;

@end

#define loadMoreImageWH         24

#define loadMoreImageX          20

#define loadMoreImageY          (0 - loadMoreImageWH - 10)

#define loadMoreImageCenter     CGPointMake(loadMoreImageX + loadMoreImageWH / 2, loadMoreImageY + loadMoreImageWH / 2)

#define loadOnScrollViewOffsetY -50

#define loadingMoreImageCenter  CGPointMake(loadMoreImageX + loadMoreImageWH / 2, loadMoreImageCenter.y - loadOnScrollViewOffsetY)

#define No_Cache_Image(imgName) [UIImage imageWithContentsOfFile:[[NSBundle mainBundle].resourcePath stringByAppendingFormat:@"/%@.png", imgName]]

#define Net_Image(imgName)      [UIImage imageWithContentsOfFile:[[NSBundle mainBundle].resourcePath stringByAppendingFormat:@"/%@", imgName]]

#define bgColor RGBCOLOR(249, 249, 248)

#define Friend_Avatar_Button_Tag    10001   // 头像按钮Tag
#define Friend_Nickname_Button_Tag    10002   // 昵称按钮Tag
#define Friend_Content_Text_Label_Tag    10003   // 发布内容Tag
#define Friend_Content_Image_View_Tag    10004   // 发布内容Tag
#define Friend_Date_Label_Tag    10005   // 发布时间Tag
#define Friend_Func_Button_Tag    10006   // 功能按钮Tag

@implementation KMainViewController

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.dataArray = [NSMutableArray array];
    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.allowsSelection = NO;
    [self.tableView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideCommentView)]];
    
    self.loadMoreImageView = [[UIImageView alloc] initWithImage:No_Cache_Image(@"friendactivity_refresh")];
    
    self.loadMoreImageView.center = loadMoreImageCenter;
    
    [self.view addSubview:self.loadMoreImageView];
    
    // 所有表情 \uE415 \uE056 \uE057 \uE414 \uE405 \uE106 \uE418 \uE417 \uE40d \uE40a \uE404 \uE105 \uE409 \uE40e \uE402 \uE108 \uE403 \uE058 \uE407 \uE401 \uE40f \uE40b \uE406 \uE413 \uE411 \uE412 \uE410 \uE107 \uE059 \uE416 \uE408 \uE40c \uE11a \uE10c \uE32c \uE32a \uE32d \uE328 \uE32b \uE022 \uE023 \uE327 \uE329 \uE32e \uE32f \uE335 \uE334 \uE021 \uE336 \uE13c \uE337 \uE020 \uE330 \uE331 \uE326 \uE03e \uE11d \uE05a \uE00e \uE421 \uE420 \uE00d \uE010 \uE011 \uE41e \uE012 \uE422 \uE22e \uE22f \uE231 \uE230 \uE427 \uE41d \uE00f \uE41f \uE14c \uE201 \uE115 \uE428 \uE51f \uE429 \uE424 \uE423 \uE253 \uE426 \uE111 \uE425 \uE31e \uE31f \uE31d \uE001 \uE002 \uE005 \uE004 \uE51a \uE519 \uE518 \uE515 \uE516 \uE517 \uE51b \uE152 \uE04e \uE51c \uE51e \uE11c \uE536 \uE003 \uE41c \uE41b \uE419 \uE41a
    
    // 评论/赞
    self.commentView = [[UIView alloc] initWithFrame:(CGRect){0, 0, 0, 46}];
    self.commentView.clipsToBounds = YES;
    [self.view addSubview:self.commentView];
    UIImageView *commentBgView = [[UIImageView alloc] initWithFrame:self.commentView.frame];
    commentBgView.image = [No_Cache_Image(@"friendactivity_comment_frame_bg") stretchableImageWithLeftCapWidth:13 topCapHeight:0];
    commentBgView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.commentView addSubview:commentBgView];
    
    self.buttonBgView = [[UIView alloc] initWithFrame:(CGRect){-8, 8, 0, 29}];
    self.buttonBgView.clipsToBounds = YES;
    [self.commentView addSubview:self.buttonBgView];
    
    // 评论
    self.commentButton = [self generateCommentButtonWithNormalImage:No_Cache_Image(@"friendactivity_comment_writeicon_normal")
                                                       pressedImage:No_Cache_Image(@"friendactivity_comment_writeicon_pressed")
                                                              title:@"评论"
                                                              frame:CGRectMake(83, 0, 75, 29)];
    [self.buttonBgView addSubview:self.commentButton];
    
    // 赞
    self.likeButton = [self generateCommentButtonWithNormalImage:No_Cache_Image(@"friendactivity_comment_likeicon_normal")
                                                    pressedImage:No_Cache_Image(@"friendactivity_comment_likeicon_pressed")
                                                           title:@"赞"
                                                           frame:CGRectMake(0, 0, 75, 29)];
    [self.buttonBgView addSubview:self.likeButton];
}

- (void) viewDidUnload
{
    [self setTableView:nil];
    [self setLoadMoreImageView:nil];
    [self setCommentButton:nil];
    [self setLikeButton:nil];
    [self setButtonBgView:nil];
    [self setCommentView:nil];
    
    [super viewDidUnload];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self startAndShowLoadingImage];
    self.insertDataArray = [KUtils getFirstData];
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - 私有方法

// 生成评论/赞按钮
- (KButton *) generateCommentButtonWithNormalImage:(UIImage *)normalImage pressedImage:(UIImage *)pressedImage title:(NSString *)title frame:(CGRect)frame
{
    UIImage *normalBgImage = [No_Cache_Image(@"friendactivity_comment_frame_normal") stretchableImageWithLeftCapWidth:5 topCapHeight:0];
    UIImage *pressedBgImage = [No_Cache_Image(@"friendactivity_comment_frame_pressed") stretchableImageWithLeftCapWidth:5 topCapHeight:0];
    
    KButton *b = [KButton buttonWithType:UIButtonTypeCustom];
    b.titleLabel.font = [UIFont systemFontOfSize:15];
    [b setBackgroundImage:normalBgImage forState:UIControlStateNormal];
    [b setBackgroundImage:pressedBgImage forState:UIControlStateHighlighted];
    [b setImage:normalImage forState:UIControlStateNormal];
    [b setImage:pressedImage forState:UIControlStateHighlighted];
    [b setTitle:title forState:UIControlStateNormal];
    [b setTitleColor:bgColor forState:UIControlStateNormal];
    b.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    b.frame = frame;
    [b addTarget:self action:@selector(pressButton:) forControlEvents:UIControlEventTouchUpInside];
    return b;
}

- (void) startAndShowLoadingImage
{
    // 是否正杂加载
    loading = YES;
    KBasicBlock completion = ^(void){
        // 自旋转动画
        CABasicAnimation* rotationAnimation;
        rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        rotationAnimation.toValue = [NSNumber numberWithFloat:360];
        rotationAnimation.duration = 30;
        rotationAnimation.repeatCount = MAXFLOAT;
        [self.loadMoreImageView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
        
        // 网络请求结束，停止
        [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(stopAndHideLoadingImage) userInfo:nil repeats:NO];
    };
    if (!CGPointEqualToPoint(self.loadMoreImageView.center, loadingMoreImageCenter)) {
        // 如果是第一次进入并需要加载时，执行此处逻辑
        [UIView animateWithDuration:.25 delay:.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.loadMoreImageView.center = loadingMoreImageCenter;
        } completion:^(BOOL finished) {
            if (finished) {
                completion();
            }
        }];
    } else {
        completion();
    }
}

- (void) stopAndHideLoadingImage
{
    loading = NO;
    
    // 移除旋转效果
    [self.loadMoreImageView.layer removeAnimationForKey:@"rotationAnimation"];
    
    // 拿到获取到的数据，并插入tablevew中
    NSMutableArray *insertIndexPath = [NSMutableArray arrayWithCapacity:self.insertDataArray.count];
    for (int i = 1; i <= self.insertDataArray.count; i ++) {
        [insertIndexPath addObject:[NSIndexPath indexPathForRow:i inSection:0]];
    }
    [self.dataArray insertObjects:self.insertDataArray atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, self.insertDataArray.count)]];
    self.insertDataArray = nil;
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:insertIndexPath withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
    
    // 隐藏状态图片
    [UIView animateWithDuration:.25 delay:.3 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.loadMoreImageView.center = loadMoreImageCenter;
    } completion:^(BOOL finished) {
        
    }];
}

- (void) showAlertMessage:(NSString *)msg
{
    [[[UIAlertView alloc] initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil] show];
}

- (NSString *) msgStringWithMsg:(KMessage *)msg
{
    NSMutableString *msgString = [NSMutableString stringWithString:msg.msg];
    if (msg.expressions && msg.expressions.count > 0) {
        for (NSString *exp in msg.expressions) {
            [msgString appendString:[KUtils emojiByString:exp]];
        }
    }
    return msgString;
}

// 计算message高度
- (CGFloat) contengViewHeightWithIndex:(NSInteger)index
{
    KMessage *msg = ((KInfo *)[self.dataArray objectAtIndex:index]).msg;
    
    CGFloat ret = [[self msgStringWithMsg:msg] sizeWithFont:K_Msg_Font constrainedToSize:CGSizeMake(252, MAXFLOAT) lineBreakMode:NSLineBreakByCharWrapping].height;
    
    if (msg.imgs == nil || msg.imgs.count == 0) {
        return ret;
    }
    
    // 加上间隔
    ret += 8;
    
    // 单张图片返回高宽比例中的高度
    if (msg.imgs.count == 1) {
        KImageInfo *img = [msg.imgs objectAtIndex:0];
        if (img.size.width > img.size.height) {
            return ret + (img.size.height * K_Msg_Image_Max_WH / img.size.width);
        }
        return ret + K_Msg_Image_Max_WH;
    }
    
    // 多张图片返回一行三张图片的固定高度
    for (int i = 0; i < msg.imgs.count; i ++) {
        if (i % 3 == 0) {
            ret += K_Msg_Image_WH;
            if (i != 0) {
                ret += 5;
            }
        }
    }
    return ret;
}

- (void) refreshImageMsgView:(UIView *)imgMsgView byIndex:(NSInteger)index
{
    for (UIView *v in imgMsgView.subviews) {
        [v removeFromSuperview];
    }
    
    CGRect contentViewFrame = imgMsgView.frame;
    contentViewFrame.size.height = 0;
    
    KMessage *msg = ((KInfo *)[self.dataArray objectAtIndex:index]).msg;
    
    if (msg.imgs == nil || msg.imgs.count == 0) {
        imgMsgView.frame = contentViewFrame;
        return ;
    }
    
    contentViewFrame.size.height += 8;
    
    if (msg.imgs.count == 1) {
        KImageInfo *img = [msg.imgs objectAtIndex:0];
        CGFloat w,h;
        if (img.size.width > img.size.height) {
            w = K_Msg_Image_Max_WH;
            h = (img.size.height * K_Msg_Image_Max_WH / img.size.width);
        } else {
            w = img.size.width * K_Msg_Image_Max_WH / img.size.height;
            h = K_Msg_Image_Max_WH;
        }
        contentViewFrame.size.width = (contentViewFrame.size.width > w) ? contentViewFrame.size.width : w;
        
        KImageMessageButton *imgButton = [self generateImageMessageButtonWithFrame:(CGRect){0, contentViewFrame.size.height, w, h} image:Net_Image(img.imgUrl)];
        [imgMsgView addSubview:imgButton];
        
        contentViewFrame.size.height += h;
        
        imgMsgView.frame = contentViewFrame;
        return ;
    }
    
    CGFloat x = 0, y = contentViewFrame.size.height;
    
    for (int i = 0; i < msg.imgs.count; i ++) {
        int j = i % 3;
        x = j * (K_Msg_Image_WH + 5);
        if (i != 0 && j == 0) {
            y += (K_Msg_Image_WH + 5);
        }
        KImageInfo *img = [msg.imgs objectAtIndex:i];
        
        KImageMessageButton *imgButton = [self generateImageMessageButtonWithFrame:(CGRect){x, y, K_Msg_Image_WH, K_Msg_Image_WH} image:Net_Image(img.imgUrl)];
        [imgMsgView addSubview:imgButton];
        
        if (i == msg.imgs.count - 1) {
            contentViewFrame.size.height = imgButton.frame.origin.y + imgButton.frame.size.height;
        }
    }
    imgMsgView.frame = contentViewFrame;
}

- (KImageMessageButton *) generateImageMessageButtonWithFrame:(CGRect)frame image:(UIImage *)image
{
    KImageMessageButton *imgButton = [[KImageMessageButton alloc] initWithFrame:frame];
    [imgButton setOnLongPressBlock:^(id arg) {
        // 长按效果
        [((KImageMessageButton *)arg) becomeFirstResponder];
        [self showMenuControllerWithMenuItems:@[[[UIMenuItem alloc] initWithTitle:@"收藏" action:@selector(collect:)]] targetRect:((KImageMessageButton *)arg).frame inView:((KImageMessageButton *)arg).superview];
    }];
    [imgButton setOnClickBlock:^(id arg) {
        // 点击效果
        [self showAlertMessage:@"点击图片，弹出大图"];
    }];
    imgButton.image = image;
    return imgButton;
}

- (void) pressButton:(UIView *)view
{
    switch (view.tag) {
        case Friend_Avatar_Button_Tag:
            [self showAlertMessage:@"点击了头像"];
            break;
        case Friend_Nickname_Button_Tag:
            [self showAlertMessage:@"点击了昵称"];
            break;
        case Friend_Func_Button_Tag: {
            // 点击功能按钮
            if (self.commentView.frame.size.width == 0) {
                KButton *funcButton = (KButton *)view;
                self.commentButton.userInfo = funcButton.userInfo;
                self.likeButton.userInfo = funcButton.userInfo;
                CGPoint p = CGPointMake(-5, view.frame.size.height / 2 - self.commentView.frame.size.height / 2);
                p = [view convertPoint:p toView:self.view];
                
                __block CGRect commentVieFrame = self.commentView.frame;
                commentVieFrame.origin = p;
                self.commentView.frame = commentVieFrame;
                commentVieFrame.size.width = 200;
                commentVieFrame.origin.x = commentVieFrame.origin.x - commentVieFrame.size.width;
                [UIView animateWithDuration:.25 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    self.commentView.frame = commentVieFrame;
                    self.buttonBgView.frame = (CGRect){8, 8, 184, 29};
                } completion:^(BOOL finished) {
                    if (finished) {
                        commentVieFrame.origin.x = commentVieFrame.origin.x + commentVieFrame.size.width - 174;
                        commentVieFrame.size.width = 174;
                        [UIView animateWithDuration:.1 animations:^{
                            self.commentView.frame = commentVieFrame;
                            self.buttonBgView.frame = (CGRect){8, 8, 158, 29};
                        }];
                    }
                }];
            } else {
                self.commentButton.userInfo = nil;
                self.likeButton.userInfo = nil;
                [self hideCommentViewWithDuration:.1];
            }
            break;
        }
    }
    
    if (view == self.likeButton) {
        KButton *funcButton = (KButton *)view;
        [self showAlertMessage:[NSString stringWithFormat:@"点击%@信息的赞按钮", ((KInfo *)[self.dataArray objectAtIndex:[funcButton.userInfo intValue]]).nickname]];
        [self hideCommentViewWithDuration:.1];
    } else if (view == self.commentButton) {
        KButton *funcButton = (KButton *)view;
        [self showAlertMessage:[NSString stringWithFormat:@"点击%@信息的评论按钮", ((KInfo *)[self.dataArray objectAtIndex:[funcButton.userInfo intValue]]).nickname]];
        [self hideCommentViewWithDuration:.1];
    }
}

// 马上隐藏评论/赞按钮
- (void) hideCommentView
{
    [self hideCommentViewWithDuration:0];
}

// 在指定时间内隐藏评论/赞按钮
- (void) hideCommentViewWithDuration:(CGFloat)duration
{
    if (self.commentView.frame.size.width > 0) {
        CGRect commentVieFrame = self.commentView.frame;
        commentVieFrame.origin.x = commentVieFrame.origin.x + commentVieFrame.size.width;
        commentVieFrame.size.width = 0;
        [UIView animateWithDuration:duration animations:^{
            self.commentView.frame = commentVieFrame;
            self.buttonBgView.frame = (CGRect){-8, 8, 0, 29};
        }];
    }
}

// 显示菜单
- (void) showMenuControllerWithMenuItems:(NSArray *)menuItems targetRect:(CGRect)targetRect inView:(UIView *)targetView
{
    UIMenuController *menuController = [UIMenuController sharedMenuController];
    menuController.menuItems = menuItems;
    [menuController setTargetRect:targetRect inView:targetView];
    [menuController setMenuVisible:YES animated:YES];
}

// 复制
- (void) copyTextMessage:(UIMenuController *)menuController
{
    [menuController setMenuVisible:NO animated:YES];
    
}

// 收藏
- (void) collect:(UIMenuController *)menuController
{
    [menuController setMenuVisible:NO animated:YES];
    
}

#pragma mark - UITableDataSource

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.dataArray && self.dataArray.count > 0) {
        return self.dataArray.count + 1;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    KTableViewCell *cell = nil;
    
    if (indexPath.row == 0) {
        static NSString *Me_Identifier = @"Me_Identifier";
        cell = [tableView dequeueReusableCellWithIdentifier:Me_Identifier];
        if (cell == nil) {
            cell = [[KTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Me_Identifier];
        }
        UIView *backgrdView = [[UIView alloc] initWithFrame:cell.frame];
        backgrdView.backgroundColor = bgColor;
        cell.backgroundView = backgrdView;
        
        // 相册封面
        UIImageView *titlePageImageView = [[UIImageView alloc] initWithImage:Net_Image(@"title_page.png")];
        titlePageImageView.frame = CGRectMake(0, -50, 320, 320);
        [cell addSubview:titlePageImageView];
        
        // 昵称
        UILabel *nickNameLabel = [[UILabel alloc] initWithFrame:(CGRect){20, 244, 204, 21}];
        nickNameLabel.font = [UIFont boldSystemFontOfSize:18];
        nickNameLabel.backgroundColor = [UIColor clearColor];
        nickNameLabel.textColor = [UIColor whiteColor];
        nickNameLabel.shadowColor = [UIColor darkGrayColor];
        nickNameLabel.textAlignment = NSTextAlignmentRight;
        nickNameLabel.shadowOffset = CGSizeMake(0, 1);
        nickNameLabel.text = @"Klaus";
        [cell addSubview:nickNameLabel];
        
        // 头像
        UIImageView *avatarImageView = [[UIImageView alloc] initWithFrame:(CGRect){240, 222, 70, 70}];
        avatarImageView.image = Net_Image(@"avatar_0.jpg");
        avatarImageView.backgroundColor = [UIColor whiteColor];
        avatarImageView.layer.borderColor = [UIColor whiteColor].CGColor;
        avatarImageView.layer.borderWidth = 2;
        
        [cell addSubview:avatarImageView];
        
        return cell;
    }
    
    // 朋友动态
    static NSString *Friend_Identifier = @"Friend_Identifier";
    
    cell = [tableView dequeueReusableCellWithIdentifier:Friend_Identifier];
    if (cell == nil) {
        cell = [[KTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Friend_Identifier];
        
        UIView *backgrdView = [[UIView alloc] initWithFrame:cell.frame];
        backgrdView.backgroundColor = bgColor;
        cell.backgroundView = backgrdView;
        
        // 头像
        KButton *avatarButton = [KButton buttonWithType:UIButtonTypeCustom];
        avatarButton.tag = Friend_Avatar_Button_Tag;
        avatarButton.adjustsImageWhenDisabled = NO;
        avatarButton.adjustsImageWhenHighlighted = NO;
        avatarButton.frame = (CGRect){10, 10, 40, 40};
        [avatarButton addTarget:self action:@selector(pressButton:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:avatarButton];
        
        // 昵称
        KButton *nicknameButton = [KButton buttonWithType:UIButtonTypeCustom];
        [nicknameButton setTitleColor:RGBCOLOR(94, 107, 146) forState:UIControlStateNormal];
        nicknameButton.titleLabel.font = [UIFont boldSystemFontOfSize:15];
        nicknameButton.tag = Friend_Nickname_Button_Tag;
        nicknameButton.frame = (CGRect){58, 10, 33, 17};
        [nicknameButton setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0xaaaaaa) size:nicknameButton.frame.size] forState:UIControlStateHighlighted];
        nicknameButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [nicknameButton addTarget:self action:@selector(pressButton:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:nicknameButton];
        
        // 文字表情内容
        KTextMessageLabel *textMsgLabel = [[KTextMessageLabel alloc] initWithFrame:(CGRect){58, 35, 252, 0}];
        [textMsgLabel setOnLongPressBlock:^(id arg) {
            [((KTextMessageLabel *)arg) becomeFirstResponder];
            [self showMenuControllerWithMenuItems:@[[[UIMenuItem alloc] initWithTitle:@"复制" action:@selector(copyTextMessage:)], [[UIMenuItem alloc] initWithTitle:@"收藏" action:@selector(collect:)]] targetRect:((KTextMessageLabel *)arg).frame inView:((KTextMessageLabel *)arg).superview];
        }];
        textMsgLabel.tag = Friend_Content_Text_Label_Tag;
        [textMsgLabel setBackgroundColor:bgColor forState:UIControlStateNormal];
        [textMsgLabel setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        textMsgLabel.font = K_Msg_Font;
        [cell addSubview:textMsgLabel];
        
        // 图片内容view
        UIView *imgMsgView = [[UIView alloc] initWithFrame:(CGRect){58, 43, 252, 0}];
        imgMsgView.tag = Friend_Content_Image_View_Tag;
        imgMsgView.backgroundColor = bgColor;
        [cell addSubview:imgMsgView];
        
        // 发布时间
        UILabel *publishDateLabel = [[UILabel alloc] initWithFrame:(CGRect){58, 0, 109, 14}];
        publishDateLabel.tag = Friend_Date_Label_Tag;
        publishDateLabel.backgroundColor = bgColor;
        publishDateLabel.textAlignment = NSTextAlignmentLeft;
        publishDateLabel.font = [UIFont systemFontOfSize:13];
        publishDateLabel.textColor = [UIColor lightGrayColor];
        [cell addSubview:publishDateLabel];
        
        // 功能按钮
        KButton *funcButton = [KButton buttonWithType:UIButtonTypeCustom];
        funcButton.tag = Friend_Func_Button_Tag;
        funcButton.frame = (CGRect){280, 53, 38, 24};
        [funcButton setImage:[UIImage imageNamed:@"friendactivity_comment_icon_normal"] forState:UIControlStateNormal];
        [funcButton setImage:[UIImage imageNamed:@"friendactivity_comment_icon_pressed"] forState:UIControlStateHighlighted];
        [funcButton addTarget:self action:@selector(pressButton:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:funcButton];
        
        // 底部间隔线
        UIView *bottomLine = [[UIView alloc] initWithFrame:(CGRect){0, cell.frame.size.height - 1, cell.frame.size.width, 1}];
        bottomLine.backgroundColor = RGBCOLOR(223, 223, 222);
        bottomLine.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
        [cell addSubview:bottomLine];
    }
    
    NSInteger index = indexPath.row - 1;
    
    KInfo *info = [self.dataArray objectAtIndex:index];
    
    KButton *avatarButton = (KButton *)[cell viewWithTag:Friend_Avatar_Button_Tag];
    avatarButton.userInfo = @(index);
    [avatarButton setImage:Net_Image(info.avatarUrl) forState:UIControlStateNormal];
    
    KButton *nicknameButton = (KButton *)[cell viewWithTag:Friend_Nickname_Button_Tag];
    nicknameButton.userInfo = @(index);
    [nicknameButton setTitle:info.nickname forState:UIControlStateNormal];
    [nicknameButton sizeToFit];
    
    NSString *msgString = [self msgStringWithMsg:info.msg];
    KTextMessageLabel *textMsgLabel = (KTextMessageLabel *)[cell viewWithTag:Friend_Content_Text_Label_Tag];
    textMsgLabel.text = msgString;
    [textMsgLabel sizeToFit];
    
    CGRect frame;
    UIView *imgMsgView = [cell viewWithTag:Friend_Content_Image_View_Tag];
    frame = imgMsgView.frame;
    frame.origin.y = textMsgLabel.frame.origin.y + textMsgLabel.frame.size.height + 8;
    imgMsgView.frame = frame;
    [self refreshImageMsgView:imgMsgView byIndex:index];
    
    UILabel *publishDateLabel = (UILabel *)[cell viewWithTag:Friend_Date_Label_Tag];
    frame = publishDateLabel.frame;
    frame.origin.y = imgMsgView.frame.origin.y + imgMsgView.frame.size.height + 8;
    publishDateLabel.frame = frame;
    publishDateLabel.text = info.publishDate;
    [publishDateLabel sizeToFit];
    
    KButton *funcButton = (KButton *)[cell viewWithTag:Friend_Func_Button_Tag];
    frame = funcButton.frame;
    frame.origin.y = publishDateLabel.frame.origin.y;
    funcButton.frame = frame;
    funcButton.userInfo = @(index);
    
    return cell;
}

#pragma mark - UITableDelegate

- (void) scrollViewDidScroll:(UIScrollView *)scrollView
{
    // 隐藏评论按钮
    [self hideCommentView];
    
    if (scrollView.contentOffset.y <= 0 && loading == NO) {
        // 根据拉动距离旋转
        self.loadMoreImageView.layer.transform = CATransform3DRotate(self.loadMoreImageView.layer.transform, ((lastY - scrollView.contentOffset.y) / 15), 0, 0, 1);
        if (scrollView.contentOffset.y <= loadOnScrollViewOffsetY) {
            // 固定位置
            self.loadMoreImageView.center = loadingMoreImageCenter;
        } else {
            // 跟随
            self.loadMoreImageView.center = CGPointMake(self.loadMoreImageView.center.x, loadMoreImageCenter.y - scrollView.contentOffset.y);
        }
        lastY = scrollView.contentOffset.y;
    }
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView.contentOffset.y <= loadOnScrollViewOffsetY) {
        // 开始加载
        [self startAndShowLoadingImage];
        self.insertDataArray = [KUtils getSecondData];
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 310;
    }
    return 83 + [self contengViewHeightWithIndex:indexPath.row - 1];
}

@end
