//
//  KMainViewController.h
//  COF
//
//  Created by corptest on 13-8-19.
//  Copyright (c) 2013年 klaus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KMainViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end
